# README #
## DSC Coding Challenge  ##

### What is this repository for? ###
### This simple program joins the user events based on the event ID and each event ID also has the link to the next event. The format of the output file ### 
### is event_id,collector_tstamp,domain_userid,page_urlpath,next_event_id ###

### The code is written in Pyhton 2.7 ###

### How do I get set up? ###

### Due to time crunch, I have not passed the file names as arguments and have assumed and hardcoded file names  ###
### The input file name is assumed as pageviews.csv and the out put file name is given as joinedpageviews.csv    ###
### The input file needs to be existing in the same directory where this program is being run and once the program is successfully executed the output file is generated int he same folder location ###

## Running Tests
### Command : 'python joinuserevents.py'	
### Output file generated is as follows  ###

\event_id,collector_tstamp,domain_userid,page_urlpath,next_event_id\
\44fc8bb2-817c-47e0-9646-79b87be1b793,2016-01-01 05:34:40,6f9c7b2df33a009d,/checkout/extras,e7d7516e-434c-451b-ab0d-91554737ae38\
\e7d7516e-434c-451b-ab0d-91554737ae38,2016-01-01 05:34:52,6f9c7b2df33a009d,/blades,e7c2b44f-6a83-4663-9e6c-ce4d0e2c3f3b\
\1abc1e81-3ae3-4bfd-897c-2b580f9e4ec1,2016-01-01 05:34:56,6f9c7b2df33a009d,/checkout/extras,e7c2b44f-6a83-4663-9e6c-ce4d0e2c3f3b\
\e7c2b44f-6a83-4663-9e6c-ce4d0e2c3f3b,2016-01-01 05:35:14,6f9c7b2df33a009d,/your-box/funnel,\
\ec194b5b-11c5-4926-9d73-3d9c357f1eaf,2016-01-01 20:35:15,96a089a556e20b04,/checkout/extras,80126d18-bfc5-4ccc-a08a-da51265ac802\
\80126d18-bfc5-4ccc-a08a-da51265ac802,2016-01-01 20:35:29,96a089a556e20b04,/your-box/funnel,a944aa8c-1e2e-4005-8a8a-dab2b59ac4a0\
\a944aa8c-1e2e-4005-8a8a-dab2b59ac4a0,2016-01-01 20:35:34,96a089a556e20b04,/blades,51d2fa46-5231-44e4-becf-916f889db41b\
\51d2fa46-5231-44e4-becf-916f889db41b,2016-01-01 20:35:56,96a089a556e20b04,/checkout/extras,\
\f3a055db-ef56-4af2-874a-b2c9a5b88e07,2016-01-01 04:33:40,b84420e631a8fe8a,/blades,4dadfb45-ede4-4939-afcf-047fa344022e\
\4dadfb45-ede4-4939-afcf-047fa344022e,2016-01-01 04:33:59,b84420e631a8fe8a,/checkout/extras,742d5b4f-0763-49f9-bd5a-340e80e08e70\
\742d5b4f-0763-49f9-bd5a-340e80e08e70,2016-01-01 04:34:06,b84420e631a8fe8a,/checkout/extras,\
\f7b6391d-0886-4eee-865e-0ef98b6ca0cc,2016-01-01 04:34:17,b84420e631a8fe8a,/your-box/funnel,\
\2a216e8e-d732-4414-b1a8-51a35875f06c,2016-01-01 13:30:50,b410f87747e8ee11,/login,3e126a4b-5653-4d14-9c37-4821707079e0\
\3e126a4b-5653-4d14-9c37-4821707079e0,2016-01-01 13:30:51,b410f87747e8ee11,/login,ab3ea336-904c-4124-b59d-d6613b456329\
\ab3ea336-904c-4124-b59d-d6613b456329,2016-01-01 13:31:02,b410f87747e8ee11,/login,3c1b9a28-b01d-43a8-b211-8b756a624770\
\4ff80274-e28d-48c9-a91d-58cc7c261a31,2016-01-01 13:31:02,b410f87747e8ee11,/login,d3b599d2-ab52-4d59-afae-fbd99b033210\
\d3b599d2-ab52-4d59-afae-fbd99b033210,2016-01-01 13:31:10,b410f87747e8ee11,/your-account,8868c1c5-5441-40a0-ac32-9a372e4c1d91\
\3c1b9a28-b01d-43a8-b211-8b756a624770,2016-01-01 13:31:11,b410f87747e8ee11,/our-products,8868c1c5-5441-40a0-ac32-9a372e4c1d91\
\05e674ee-adb0-44ce-876e-187ce4f1f6c4,2016-01-01 13:31:12,b410f87747e8ee11,/our-products/style,8868c1c5-5441-40a0-ac32-9a372e4c1d91\
\8868c1c5-5441-40a0-ac32-9a372e4c1d91,2016-01-01 13:31:12,b410f87747e8ee11,/login,2f484d44-2088-4af2-a178-4bd99281a4cf\
\2f484d44-2088-4af2-a178-4bd99281a4cf,2016-01-01 13:31:13,b410f87747e8ee11,/our-products,6d008c5e-7221-4feb-b5cc-35345aa3301a\
\6d008c5e-7221-4feb-b5cc-35345aa3301a,2016-01-01 13:31:21,b410f87747e8ee11,/our-products,25176877-a6d2-4da2-80e6-ff163cd0d279\
\99daf3c6-bcb2-46cf-8dd2-78e63f06bb1a,2016-01-01 13:31:22,b410f87747e8ee11,/our-products,d46ed7e6-6a52-495f-a612-a7376b806f40\
\d46ed7e6-6a52-495f-a612-a7376b806f40,2016-01-01 13:31:24,b410f87747e8ee11,/our-products,d735aa71-aee1-41a4-b0c9-6e9815d344b0\
\25176877-a6d2-4da2-80e6-ff163cd0d279,2016-01-01 13:31:24,b410f87747e8ee11,/our-products/fresh,d735aa71-aee1-41a4-b0c9-6e9815d344b0\
\d735aa71-aee1-41a4-b0c9-6e9815d344b0,2016-01-01 13:31:25,b410f87747e8ee11,/our-products,a6fa5c05-f1ef-418e-ae04-f8701ec997da\
\d38f012c-87fe-49f7-8349-a3192dc1811c,2016-01-01 13:31:30,b410f87747e8ee11,/our-products,a6fa5c05-f1ef-418e-ae04-f8701ec997da\
\983fdf40-6d8e-4c0a-a1a1-d8aa3a5a9b54,2016-01-01 13:31:30,b410f87747e8ee11,/your-box/add-product/hair-cream,656b3f37-d98a-48a4-9a5e-20b9841be2ed\
\656b3f37-d98a-48a4-9a5e-20b9841be2ed,2016-01-01 13:31:31,b410f87747e8ee11,/our-products,b2e37f26-79ce-4a6d-9be4-9df461d3fec4\
\a6fa5c05-f1ef-418e-ae04-f8701ec997da,2016-01-01 13:31:31,b410f87747e8ee11,/your-box/add-product/travel-wipes,b2e37f26-79ce-4a6d-9be4-9df461d3fec4\
\63970b7d-8b8c-4112-badf-e6850a961a6b,2016-01-01 13:31:31,b410f87747e8ee11,/our-products,04195a2b-5890-4403-baa9-59b9cb3eae41\
\04195a2b-5890-4403-baa9-59b9cb3eae41,2016-01-01 13:31:34,b410f87747e8ee11,/your-box/add-product/travel-wipes/qty/1,3bf19b11-ccdf-4415-8a1b-663ef40fdc85\
\b2e37f26-79ce-4a6d-9be4-9df461d3fec4,2016-01-01 13:31:34,b410f87747e8ee11,/your-box/next,3bf19b11-ccdf-4415-8a1b-663ef40fdc85\
\0dd34ec3-0fdd-4328-a0cf-01bf2ed393d2,2016-01-01 13:31:35,b410f87747e8ee11,/cancel-subscription,3bf19b11-ccdf-4415-8a1b-663ef40fdc85\
\e4050ba2-4d75-4f4b-ab93-e8bb8c4bf187,2016-01-01 13:31:37,b410f87747e8ee11,/your-account,3bf19b11-ccdf-4415-8a1b-663ef40fdc85\
\c8c561e3-a268-40dc-8a9f-ced5a3e7d563,2016-01-01 13:31:38,b410f87747e8ee11,/our-products/fresh,be9e6eae-917c-49b7-aae2-3824dce2f35b\
\be9e6eae-917c-49b7-aae2-3824dce2f35b,2016-01-01 13:31:40,b410f87747e8ee11,/membership-settings,e26ba890-d101-4534-a04a-5ed88c06bf97\
\3e3d12a4-0d5e-456e-ae65-910ea4841507,2016-01-01 13:31:41,b410f87747e8ee11,/your-box/add-product/wipes,a884ca04-c893-4cd9-8b3b-294897f4d653\
\a884ca04-c893-4cd9-8b3b-294897f4d653,2016-01-01 13:31:42,b410f87747e8ee11,/your-box/add-product/hair-cream/qty/1,417bc7a9-c8e1-496d-95e0-acf6a47d22e7\
\417bc7a9-c8e1-496d-95e0-acf6a47d22e7,2016-01-01 13:31:43,b410f87747e8ee11,/membership-settings,3d3c9d17-2872-4f13-8804-0f1394879ea3\
\b7b3e6ee-79ff-4277-bf39-8ac2735a02fb,2016-01-01 13:31:44,b410f87747e8ee11,/your-box/add-product/wipes/qty/1,3d3c9d17-2872-4f13-8804-0f1394879ea3\
\3d3c9d17-2872-4f13-8804-0f1394879ea3,2016-01-01 13:31:45,b410f87747e8ee11,/cancel-subscription,320e0641-fb7f-4656-81c0-cb8b28296e0a\
\a3794fdd-3326-40e5-a656-365fc9c8d0ae,2016-01-01 13:31:48,b410f87747e8ee11,/our-products/style,320e0641-fb7f-4656-81c0-cb8b28296e0a\
\320e0641-fb7f-4656-81c0-cb8b28296e0a,2016-01-01 13:31:49,b410f87747e8ee11,/our-products/fresh,fdc1721f-4b60-4304-a2fb-9e9e0895e1e6\
\e26ba890-d101-4534-a04a-5ed88c06bf97,2016-01-01 13:31:49,b410f87747e8ee11,/our-products/fresh,fdc1721f-4b60-4304-a2fb-9e9e0895e1e6\
\3bf19b11-ccdf-4415-8a1b-663ef40fdc85,2016-01-01 13:31:51,b410f87747e8ee11,/our-products,f83c8efa-259c-4c3b-8e87-2070dd587a73\
\f83c8efa-259c-4c3b-8e87-2070dd587a73,2016-01-01 13:31:52,b410f87747e8ee11,/your-box/item/MNgCneCQnBTsAhpUSKW9lFF4HVq0-nvtRoKs8OI38JUARmGfvzXn6J96T-vZVtPV/confirm-remove,b15328ef-8f81-46e1-afe0-0afb97739bed\